from scapy.all import *
from datetime import datetime, timedelta
import time

def response(pkt):
    # record time receive the packet
    results.append([pkt, datetime.now()])
    if len(results) >= 5:
        op = ""
        for t in results:
            op += "bot {} with {}ms ping distance -> arrived at {}\n". format(
                    t[0][IP].src, t[0][Raw].load, t[1].strftime('%H:%M:%S.%f'))
        with open("test_result",'a') as fout:
            fout.write(op)
        print op

if __name__== '__main__':
    results = []
    sniff(filter="udp and port 668", prn=response)
