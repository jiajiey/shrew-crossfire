# 18-731 Network Security - HW4 DNS Attack
# Network Topology Setup Script

from mininet.net import Mininet
from mininet.topo import Topo
from mininet.node import Controller
from mininet.cli import CLI
from mininet.link import Intf, TCLink
from mininet.log import setLogLevel, info
from mininet.node import RemoteController
import random
import time


class myTopo(Topo):

    def build(self):
        info( '*** Add switches\n')
        # s1 attaches decoy server
        s1 = self.addSwitch('s1')
        s2 = self.addSwitch('s2')
        # s3 and s4 attaches bots
        s3 = self.addSwitch('s3')
        s4 = self.addSwitch('s4')

        info( '*** Add hosts\n')
        h1 = self.addHost('bot1')
        h2 = self.addHost('bot2')
        h3 = self.addHost('bot3')
        h4 = self.addHost('bot4')
        h5 = self.addHost('bot5')
        h6 = self.addHost('bot6')
        h7 = self.addHost('decoy')

        info( '*** Add links\n')
        self.addLink(s1, s2, bw=100, delay='10ms')
        self.addLink(s2, s3, bw=100, delay='5ms')
        self.addLink(s2, s4, bw=100, delay='45ms')
        self.addLink(h1, s3, bw=20, delay=str(random.randint(10,100))+'ms')
        self.addLink(h2, s3, bw=20, delay=str(random.randint(10,100))+'ms')
        self.addLink(h3, s3, bw=20, delay=str(random.randint(10,100))+'ms')
        self.addLink(h4, s4, bw=20, delay=str(random.randint(10,100))+'ms')
        self.addLink(h5, s4, bw=20, delay=str(random.randint(10,100))+'ms')
        self.addLink(h6, s4, bw=20, delay=str(random.randint(10,100))+'ms')
        self.addLink(h7, s1, bw=20, delay='7ms')


        '''
        self.addLink(s1, s2, bw=100, delay='100ms')
        self.addLink(s2, s3, bw=100, delay='150ms')
        self.addLink(s2, s4, bw=100, delay='150ms')
        self.addLink(h1, s3, bw=20, delay='100ms')
        self.addLink(h2, s3, bw=20, delay='500ms')
        self.addLink(h3, s3, bw=20, delay='10ms')
        self.addLink(h4, s4, bw=20, delay='100ms')
        self.addLink(h5, s4, bw=20, delay='300ms')
        self.addLink(h6, s4, bw=20, delay='200ms')
        self.addLink(h7, s1, bw=20, delay='5ms')
        '''



def myNetwork():

    topo = myTopo()
    net = Mininet(topo=topo, link=TCLink)

    info( '*** Starting network\n')
    net.start()

    # net.pingAll()

    decoy = net.get('decoy')
    decoy.cmd('python server.py &')

    controller = net.get('bot1')
    controller.cmd('python bot-controller.py &')

    for i in range(2,7):
        host = net.get('bot'+str(i))
        host.cmd('python bot-client.py '+host.IP()+' '+controller.IP()+' '+decoy.IP()+' &')
        print 'python bot-client.py '+host.IP()+' '+controller.IP()+' '+decoy.IP()+' &'

    CLI(net)
    net.stop()

if __name__ == '__main__':
    setLogLevel( 'info' )
    myNetwork()
