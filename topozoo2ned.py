import xml.etree.ElementTree as ET
import xml.dom.minidom as minidom
import sys
import re

NAMESPACE = '{http://graphml.graphdrawing.org/xmlns}'
LOCATION_DICT = dict()
MAX_LONG = -180;
MIN_LONG = 180;
MAX_LAT = -90;
MIN_LAT = 90;
BASE_INDEX = 0;


### Host and Link Class Definition

class Host:
    # longtitude = key: d33
    # latitude = key: d29
    # country = key: d30
    # label = key: d34
    # type = key: d31

    def __init__(self, id=None, longtitude=None, latitude=None, country=None, label=None, type=None):
        self.id = id
        self.long = longtitude
        self.lat = latitude
        self.country = country
        self.name = label
        self.type = type

class Connection:
    # type = key: d35

    def __init__(self, source=None, dest=None, type=None):
        self.src = source
        self.dest = dest
        self.type = type



### Helper functions for writing XML output

# Assignment expression
def SetLiteral(paramNode, paramName, value):
    paramNode.set('name', paramName)
    exp = ET.SubElement(paramNode, 'expression')
    exp.set('target', 'value')
    lit = ET.SubElement(exp, 'literal')
    lit.set('type', 'quantity')
    lit.set('text', value)
    lit.set('value', value)

# Integer property expression
def SetInt(paramNode, paramName, value):
    paramNode.set('name', paramName)
    paramNode.set('type', 'int')
    paramNode.set('is-default', 'true');
    exp = ET.SubElement(paramNode, 'expression')
    exp.set('target', 'value')
    lit = ET.SubElement(exp, 'literal')
    lit.set('type', 'int')
    lit.set('text', value)
    lit.set('value', value)

# Display String
def SetDisplayParam(params, displayStr):
    property = ET.SubElement(params, 'property')
    property.set('name', 'display')

    propertyKey = ET.SubElement(property, 'property-key')
    literal = ET.SubElement(propertyKey, 'literal')
    literal.set('type', 'string')

    literal.set('text', displayStr)
    literal.set('value', displayStr)

# Generate Host Array
def SetHostArray(module, name, quantityName, displayParam):
    submodule = ET.SubElement(module, 'submodule')
    submodule.set('name', name)
    submodule.set('type', 'StandardHost')

    exp = ET.SubElement(submodule, 'expression')
    exp.set('target','vector-size')
    ident = ET.SubElement(exp, 'ident')
    ident.set('name', quantityName)

    params = ET.SubElement(submodule, 'parameters')
    SetDisplayParam(params, displayParam)

# Setup connection between host-router or router-router
def SetConnection(module, type, source, dest):
    connection = ET.SubElement(module, 'connection')
    connection.set('src-module', source)
    connection.set('src-gate', 'pppg')
    connection.set('src-gate-plusplus', 'true')
    connection.set('dest-module', dest)
    connection.set('dest-gate', 'pppg')
    connection.set('dest-gate-plusplus', 'true')
    connection.set('type', type)
    connection.set('is-bidirectional', 'true')
    return connection

# Setup connection between array of host to router
def generateHostAttachedToRouter(hostArrayName, hostID, routerID, tree):
    connections = tree.iter('connections').next()
    conn = SetConnection(connections, 'BLUE', hostArrayName, LOCATION_DICT[routerID])
    exp = ET.SubElement(conn, 'expression')
    exp.set('target', 'src-module-index')
    lit = ET.SubElement(exp, 'literal')
    lit.set('type', 'int')
    lit.set('text', str(hostID))
    lit.set('value', str(hostID))

# Generate Link Type Class
def DefineLinkType(rootNode, name, datarate, delay):
    rootNode.set('name', name)

    extends = ET.SubElement(rootNode, 'extends')
    extends.set('name', 'DatarateChannel')

    params = ET.SubElement(rootNode, 'parameters')
    params.set('is-implicit', 'true')

    dataRateParam = ET.SubElement(params, 'param')
    SetLiteral(dataRateParam, 'datarate', datarate)

    delayParam = ET.SubElement(params, 'param')
    SetLiteral(delayParam, 'delay', delay)

    perParam = ET.SubElement(params, 'param')
    SetLiteral(perParam, 'per', '0')

    berParam = ET.SubElement(params, 'param')
    SetLiteral(berParam, 'ber', '0')




### Helper functions to read property from source XML file

def SwitchProperty(id, key, value, host):
    if value is None:
        return

    global MIN_LAT
    global MAX_LAT
    global MIN_LONG
    global MAX_LONG

    if key == 1:
        value = float(value)
        if value < MIN_LAT:
            MIN_LAT = value
        if value > MAX_LAT:
            MAX_LAT = value
        host.lat = value
    elif key == 2:
        host.country = value
    elif key == 3:
        host.type = value
    elif key == 5:
        value = float(value)
        if value < MIN_LONG:
            MIN_LONG = value
        if value > MAX_LONG:
            MAX_LONG = value
        host.long = value
    elif key == 6:
        value = re.sub(r'\W+', '', value)
        host.name = value
        LOCATION_DICT[id] = value

def ReadXML(sourceName):
    tree = ET.parse(sourceName)
    return tree.getroot()

def GenerateNodes(root):
    global BASE_INDEX

    nodeList = list()
    for node in root.iter(NAMESPACE+'node'):
        host = Host()
        id = node.attrib['id']
        properties = node.findall(NAMESPACE+'data')
        try:
            if BASE_INDEX == 0:
                BASE_INDEX = int(properties[0].attrib['key'][1:])
            for property in properties:
                index = int(property.attrib['key'][1:]) - BASE_INDEX
                SwitchProperty(id, index, property.text, host)
            nodeList.append(host)
        except ValueError:
            # unordered data, ignore this node
            continue
    return nodeList

def GenerateLinks(root):
    linkList = list()
    for link in root.iter(NAMESPACE+'edge'):
        conn = Connection(link.attrib['source'], link.attrib['target'])
        for property in link.findall(NAMESPACE+'data'):
            if property.attrib['key'] == 'd35':
                conn.type = property.text
        linkList.append(conn)
    return linkList



### Generate result XML used for NED file

def GenerateNedXML(nodes, links, coreName):

    xRange = MAX_LONG - MIN_LONG + 20;
    yRange = MAX_LAT - MIN_LAT + 10;

    root = ET.Element('ned-file')
    root.set('filename',coreName + '.ned')

    # imports

    import1 = ET.SubElement(root, 'import')
    import1.set('import-spec', 'inet.node.inet.StandardHost')

    import2 = ET.SubElement(root, 'import')
    import2.set('import-spec', 'ned.DatarateChannel')

    import3 = ET.SubElement(root, 'import')
    import3.set('import-spec', 'inet.node.inet.Router')

    import4 = ET.SubElement(root, 'import')
    import4.set('import-spec', 'inet.networklayer.configurator.ipv4.IPv4NetworkConfigurator')

    # network

    network = ET.SubElement(root, 'compound-module')
    network.set('name', coreName)

    params = ET.SubElement(network, 'parameters')
    isNetwork = ET.SubElement(params, 'property')
    isNetwork.set('is-implicit', 'true')
    isNetwork.set('name', 'isNetwork')

    SetDisplayParam(params, '"bgb=2000,1600;bgi=maps/china,s;bgg=1000,2,grey95;bgu=km"')

    # define types

    types = ET.SubElement(network, 'types')
    channel1 = ET.SubElement(types, 'channel')
    DefineLinkType(channel1, 'RED', '10Mbps', '10ms')
    channel2 = ET.SubElement(types, 'channel')
    DefineLinkType(channel2, 'BLUE', '10Mbps', '10ms')

    # Routers + Configurator

    submodules = ET.SubElement(network, 'submodules')

    submodule = ET.SubElement(submodules, 'submodule')
    submodule.set('name', 'configurator')
    submodule.set('type', 'IPv4NetworkConfigurator')
    params = ET.SubElement(submodule, 'parameters')
    SetDisplayParam(params, '"p=100,100"')

    for node in nodes:
        submodule = ET.SubElement(submodules, 'submodule')
        submodule.set('name', node.name)
        submodule.set('type', 'Router')

        params = ET.SubElement(submodule, 'parameters')

        if node.long is not None and node.lat is not None:
            normalizedLong = str(( node.long - MIN_LONG ) * 2300.0 / xRange + 200)
            normalizedLat = str(( MAX_LAT - node.lat + MIN_LAT ) * 1800.0 / yRange - 750)
            SetDisplayParam(params, '"i=abstract/router_vs;p='+normalizedLong+','+normalizedLat+'"')
        else:
            SetDisplayParam(params, '"i=abstract/router_vs"')

    # Links

    connections = ET.SubElement(network, 'connections')
    for conn in links:
        if conn.src not in LOCATION_DICT or conn.dest not in LOCATION_DICT:
            continue
        SetConnection(connections, 'RED', LOCATION_DICT[conn.src], LOCATION_DICT[conn.dest])

    tree = ET.ElementTree(root)
    return tree



### Additional Function: Pick degree=1 router and add host
def addHostToLonelyRouter(tree, links):

    candidateHostList = set()
    badHostList = set()

    for conn in links:
        if conn.src not in LOCATION_DICT or conn.dest not in LOCATION_DICT:
            continue
        tryAddCandidate(conn.src, candidateHostList, badHostList)
        tryAddCandidate(conn.dest, candidateHostList, badHostList)

    # now hosts in candidate list are the lonely routers we want

    hostIndex = 0
    numHosts = len(candidateHostList)

    # generate number of bots and add to network parameter

    networkParams = tree.iter('compound-module').next().find('parameters')
    numBotsParam = ET.SubElement(networkParams, 'param')
    SetInt(numBotsParam, "numBots", str(numHosts))

    # add the Bots array

    submodules = tree.iter('submodules').next()
    SetHostArray(submodules, 'Bots', 'numBots', '"i=device/pc2_vs,red,25"')

    for host in candidateHostList:
        generateHostAttachedToRouter("Bots", str(hostIndex), host, tree)
        hostIndex += 1

def tryAddCandidate(id, candidateHostList, badHostList):
    if id in candidateHostList:
        candidateHostList.remove(id)
        badHostList.add(id)
    else:
        if id in badHostList:
            return
        else:
            candidateHostList.add(id)



### Try to get output pretty

def indent(elem, level=0):
    i = "\n" + level*"  "
    if len(elem):
        if not elem.text or not elem.text.strip():
            elem.text = i + "  "
        if not elem.tail or not elem.tail.strip():
            elem.tail = i
        for elem in elem:
            indent(elem, level+1)
        if not elem.tail or not elem.tail.strip():
            elem.tail = i
    else:
        if level and (not elem.tail or not elem.tail.strip()):
            elem.tail = i
    return ET.ElementTree(elem)

def prettify(elem):
    rough_string = ET.tostring(elem, 'utf-8')
    reparsed = minidom.parseString(rough_string)
    return reparsed.toprettyxml(indent="\t")

if __name__ == '__main__' :
    if len(sys.argv) < 2:
        print "Usage: topozoo2ned.py [core name]"

    coreName = sys.argv[1]

    xmlRoot = ReadXML(coreName + '.xml')
    nodes = GenerateNodes(xmlRoot)
    links = GenerateLinks(xmlRoot)
    resultXmlTree = GenerateNedXML(nodes, links, coreName)
    addHostToLonelyRouter(resultXmlTree, links)
    resultXML = prettify(resultXmlTree.getroot())
    with open(coreName + '-result.xml', 'w') as output:
        output.write(resultXML)
#    resultXmlTree.write(coreName + '-result.xml')
