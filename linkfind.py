
DEBUG = True
INIOUTPUT = True 
linkDelay = 10
linkDelayUnit = "ms"

path = []
p = []
while True:
    try:
        line = raw_input().split()
        if len(line) == 0: continue
        if (line[4].lower() == "server"):
            path.append(p)
            p = []
            continue
        else:
            p.append("{0}-->{1}".format(line[2], line[4]))
    except EOFError:
        break

if DEBUG:
    print "Extracted paths:"
    for i in path: print i
    print '-' * 20

target = []
while len(path) > 0:
    linkCount = {}
    linkNear = {}
    maxlink = ""
    maxlinkCount = 0
    maxlinkNear = 100
    for link in path:
        for i, pair in enumerate(link):
            if i == 0: continue
            if linkCount.setdefault(pair, None) is None:
                linkCount[pair] = 1
                linkNear[pair] = i
            else:
                linkCount[pair] += 1
                linkNear[pair] += i
            if ((linkCount[pair] > maxlinkCount) or
                    (linkCount[pair] == maxlinkCount and
                        linkNear[pair] < maxlinkNear)):
                maxlink = pair
                maxlinkCount = linkCount[pair]
                maxlinkNear = linkNear[pair]
    target.append([maxlink, 0])
    i = 0
    while i < len(path):
        if maxlink in path[i]:
            target[-1].append(path[i][0].split("-->")[0])
            target[-1].append(len(path[i]))
            if target[-1][1] < target[-1][-1]: target[-1][1] = target[-1][-1]
            path.remove(path[i])
        else:
            i += 1

print "The target links are as follows:"
for i in target:
    for j in xrange(0, len(i), 2):
        if (j == 0):
            print "{}, used by: ".format(i[j]),
        else:
            print "{0}, delay:{1}|".format(i[j], i[1]-i[j+1]),
    print

if INIOUTPUT:
    print '-' * 20
    for index, i in enumerate(target):
        for j in xrange(2, len(i), 2):
            print "**.{0}.**.startTime = ${{st{1}}}s + {2} * {3}{4}".format(
                    i[j], index, i[1]-i[j+1], linkDelay, linkDelayUnit)
        print
